# Contributing

Thank you for being interested in contributing! This document should help you
get started with contributing to ienv.

## Reporting issues

Issues can be reported on the [issue
tracker](https://gitlab.com/inko-lang/ienv/issues). When reporting a bug, make
sure to use the "bug" issue template.

## Submitting merge requests

When submitting a merge request, please provide as much detail as possible in
the merge request description. By providing as much information as possible,
reviewing your changes will be much easier.

## Style guide

* Keep lines at roughly 80 characters per line, though it's OK if they are a
  little bit longer (e.g. when using string literals).
* Use [shellcheck](https://www.shellcheck.net/) whenever changing `bin/ienv`.
* Avoid using GNU specific extensions that don't have any BSD counterparts, as
  this makes it harder to support BSD and Mac OS X.
* Keep things as simple as possible, and keep the number of options down to an
  absolute minimum.
* Write good commit messages. See <https://chris.beams.io/posts/git-commit/>
  and <https://github.com/agis/git-style-guide#commits> for more information. In
  short: 50 characters on the first line (no period). More details can be added
  by adding an empty line, followed by a more in-depth description of 72
  characters per line.

As a reminder, all contributors are expected to follow our [Code of
Conduct](https://inko-lang.org/code-of-conduct/).

Describe your bug here, in as much detail as possible. Make add the requested
details (e.g. the operating system) below, then remove this paragraph.

## Expected behaviour

Describe what you expected to happen here.

## Actual behaviour

Describe what happened instead.

* Operating system: ADD_OPERATING_SYSTEM_HERE
* Bash version (see `bash --version`): ADD_BASH_VERSION_HERE

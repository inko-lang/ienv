VERSION != grep -oP "version='\K(\d\.\d\.\d)" bin/ienv

tag:
	git tag -a -m "Release ${VERSION}" "v${VERSION}"
	git push --tags

.PHONY: tag

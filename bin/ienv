#!/usr/bin/env bash

# The version of ienv.
version='0.7.1'

if [[ "$XDG_DATA_HOME" == "" ]]
then
    XDG_DATA_HOME="$HOME/.local/share"
fi

if [[ "$XDG_CONFIG_HOME" == "" ]]
then
    XDG_CONFIG_HOME="$HOME/.config"
fi

if [[ "$XDG_CACHE_HOME" == "" ]]
then
    XDG_CACHE_HOME="$HOME/.cache"
fi

# The directory containing configuration data.
config_dir="${XDG_CONFIG_HOME}/ienv"

# The base directory for cached files.
cache_dir="${XDG_CACHE_HOME}/ienv"

# The path to the cached manifest file.
manifest_file="${cache_dir}/manifest.txt"

# The directory to store downloaded packages in.
downloads_dir="${cache_dir}/downloads"

# The directory to store installed versions in.
installs_dir="${XDG_DATA_HOME}/ienv/installed"

# The base URL to use for downloading packages and checksums.
mirror="${IENV_MIRROR:-https://releases.inko-lang.org}"

# The default version to use, if any.
default_version_file="${config_dir}/inko-version"

function info() {
    echo -e "\\033[1m\\033[32m>>>\\033[0m\\033[0m ${1}"
}

function error() {
    echo -e "\\033[1m\\033[31m!!!\\033[0m\\033[0m ${1}"
    exit 1
}

# Prints the usage message to STDOUT.
function usage() {
    cat <<USAGE
Usage: ienv [OPTIONS] [COMMAND] [ARGS]

Options:

    -h, --help       Display this message
    -v, --version    Display the version

Commands:

    list                    Lists all installed versions
    known                   Lists all available versions
    install [VERSION]       Installs a version of Inko
    remove [VERSION]        Removes an installed version
    run [COMMAND] [ARGS]    Runs a command using a specific version
    clean                   Removes all temporary data
    default [VERSION]       Sets the default version to use

Examples:

    ienv install 0.1.0
    ienv remove 0.1.0
    ienv list
    ienv known
    ienv run 0.1.0 inko --version
USAGE
}

# Prints the version to STDOUT.
function version() {
    echo "ienv ${version}"
}

# Creates the various directories necessary by ienv.
function create_directories() {
    mkdir -p "${config_dir}" "${cache_dir}" "${downloads_dir}" "${installs_dir}"
}

# Prints the default version to use.
function default_version() {
    local version=''

    if [[ -f "${default_version_file}" ]]
    then
        version="$(cat "${default_version_file}")"
    fi

    if [[ "${version}" == 'latest' ]]
    then
        latest_installed_version
    else
        echo "${version}"
    fi
}

# Prints all the versions from the manifest.
function available_versions() {
    cut -d '-' -f 2 "${manifest_file}" | sort -u
}

# Prints all installed versions.
function installed_versions() {
    find "${installs_dir}" -maxdepth 1 -type d -name '*.*.*' \
        -exec basename {} \; | sort -n
}

# Prints the latest version available.
function latest_version() {
    update_manifest

    local version
    version="$(available_versions | tail -n1)"

    if [[ "${version}" == '' ]]
    then
        error 'There are no versions available to install'
    fi

    echo "$version"
}

# Prints the latest version installed.
function latest_installed_version() {
    update_manifest

    local version
    version="$(installed_versions | tail -n1)"

    if [[ "${version}" == '' ]]
    then
        error 'There are no versions available to install'
    fi

    echo "$version"
}

# Returns 1 if the package is available, 0 otherwise.
function package_available() {
    if [[ ! -f "${manifest_file}" ]]
    then
        return 1
    fi

    if [[ "$(grep "${1}" "${manifest_file}")" == "${1}" ]]
    then
        return 0
    else
        return 1
    fi
}

# Checks if a command is executable or not.
function executable_exists() {
    if [[ ! -x "$(command -v "$1")" ]]
    then
        return 1
    else
        return 0
    fi
}

# Checks if a version is installed.
function version_installed() {
    local version_dir="${installs_dir}/${1}"

    if [[ -d "${version_dir}" ]]
    then
        return 0
    else
        return 1
    fi
}

# Verifies if a version is in the correct format, erroring if it's not.
function verify_version_format() {
    if [[ ! "${1}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]
    then
        error "The version \"${1}\" is not valid"
    fi
}

# Runs a Make command.
function run_make() {
    if executable_exists 'gmake'
    then
        # On Mac OS, installing Make with Homebrew will result in the executable
        # being called "gmake".
        gmake "$@"
    else
        make "$@"
    fi
}

# Makes sure all requirements are met.
function check_requirements() {
    if ! executable_exists 'rustc'
    then
        error 'rustc is not in PATH, make sure Rust is installed.'
    fi

    if ! executable_exists 'cargo'
    then
        error 'cargo is not in PATH, make sure Cargo is installed.'
    fi

    if ! executable_exists 'curl'
    then
        error 'curl is not in PATH, make sure Curl is installed.'
    fi

    if ! executable_exists 'grep'
    then
        error 'grep is not in PATH, make sure grep is installed'
    fi

    if ! executable_exists 'cut'
    then
        error 'cut is not in PATH, make sure coreutils is installed'
    fi

    if ! executable_exists 'find'
    then
        error 'find is not in PATH, make sure GNU findutils is installed'
    fi

    if ! executable_exists 'cut'
    then
        error 'cut is not in PATH, make sure GNU coreutils is installed'
    fi

    if ! executable_exists 'sort'
    then
        error 'sort is not in PATH, make sure GNU coreutils is installed'
    fi

    if ! executable_exists 'basename'
    then
        error 'basename is not in PATH, make sure GNU coreutils is installed'
    fi

    if ! executable_exists 'make'
    then
        if ! executable_exists 'gmake'
        then
            error 'make is not in PATH, make sure Make is installed.'
        fi
    fi

    if ! executable_exists 'ruby'
    then
        error 'ruby is not in PATH, make sure Ruby is installed.'
    fi

    if [[ "$(run_make --version | grep 'Make 4.')" == "" ]]
    then
        error 'Make 4.0 or newer is required'
    fi

    # Make sure the user has at Ruby 2.3 or newer installed.
    local ruby_version
    ruby_version="$(ruby --version | cut -d ' ' -f 2)"

    if [[ "$(echo "${ruby_version}" | cut -d '.' -f 1)" != "2" ]]
    then
        error 'You need at least Ruby 2.3 or newer to install the compiler'
    fi

    if [[ "$(echo "${ruby_version}" | cut -d '.' -f 2)" -lt "3" ]]
    then
        error 'You need at least Ruby 2.3 or newer to install the compiler'
    fi
}

# Updates the local copy of the manifest.
function update_manifest() {
    local curl_time_cond=""

    # If the manifest file already exists we only want to download a new version
    # if the contents (remotely) have changed.
    if [[ -f "${manifest_file}" ]]
    then
        curl_time_cond="-z ${manifest_file}"
    fi

    # If the local manifest is less than a day old, don't both updating it at
    # all. This reduces the amount of HTTP requests going to the mirror.
    if [[ ! -f "${manifest_file}" || "$(find "${manifest_file}" -mtime +1)" != "" ]]
    then
        # shellcheck disable=SC2086
        curl --silent --output "${manifest_file}" \
            $curl_time_cond "${mirror}/manifest.txt"
    fi
}

# Runs the "known" command.
function run_known() {
    update_manifest

    local versions
    versions="$(available_versions)"

    local default_version
    default_version="$(default_version)"

    if [[ "${versions}" == "" ]]
    then
        echo 'No versions are available'
    else
        for version in $versions
        do
            if [[ "${version}" == "${default_version}" ]]
            then
                if version_installed "${version}"
                then
                    echo "${version} (default)"
                else
                    echo "${version} (default, not installed)"
                fi
            else
                echo "${version}"
            fi
        done
    fi
}

# Installs a new version of Inko.
function run_install() {
    local version="${1}"

    if [[ "${version}" == "" ]]
    then
        error 'You must specify a version to install'
    fi

    if [[ "${version}" == 'latest' ]]
    then
        version="$(latest_version)"
    fi

    verify_version_format "${version}"

    if [[ -d "${installs_dir}/${version}" ]]
    then
        error "Version ${version} is already installed"
    fi

    local source_pkg="inko-${version}-source.tar.gz"

    if package_available "${source_pkg}"
    then
        info "Installing ${version}"
        install_source "${version}" "${source_pkg}"
    else
        error "No package is available for version ${version}"
    fi
}

# Removes an existing version of Inko.
function run_remove() {
    local version="${1}"

    if [[ "${version}" == "" ]]
    then
        error 'You must specify a version to remove'
    fi

    if [[ "${version}" == 'latest' ]]
    then
        version="$(latest_installed_version)"
    fi

    verify_version_format "${version}"

    local directory="${installs_dir}/${version}"

    if [[ -d "${directory}" ]]
    then
        rm -rf "${directory}"
        info "Removed version ${version}"
    else
        info "Version ${version} was not installed"
    fi
}

# Removes any temporary files.
function run_clean() {
    info 'Removing downloaded packages'

    rm -rf "${downloads_dir:?}"/*

    info 'Removing manifest'

    rm -f "${manifest_file}"
}

# Lists all installed versions.
function run_list() {
    local installed
    installed="$(installed_versions)"

    local default_version
    default_version="$(default_version)"

    for version in $installed
    do
        if [[ "${version}" == "${default_version}" ]]
        then
            echo "${version} (default)"
        else
            echo "${version}"
        fi
    done
}

# Sets the default version to use.
function run_default() {
    local version="${1}"

    if [[ "${version}" == '' ]]
    then
        error 'You must specify the version to use as the default'
    fi

    if [[ "${version}" != 'latest' ]]
    then
        verify_version_format "${version}"

        if ! version_installed "${version}"
        then
            error "Version ${version} is not installed"
        fi
    fi

    echo "${version}" > "${default_version_file}"
}

# Runs a command using the given version.
function run_run() {
    local version command

    # run -- inko --help
    if [[ "${1}" == '--' ]]
    then
        if [[ -f "${default_version_file}" ]]
        then
            version="$(cat "${default_version_file}")"
        else
            local version_override="${PWD}/inko-version"

            if [[ -f "${version_override}" ]]
            then
                version="$(cat "${version_override}")"
            else
                error 'Could not determine which version to use.

Make sure a default version is set using "ienv default X", with X being the desired version.

For example, to set 0.2.0 (assuming it is installed) as the default version, run the following:

    ienv default 0.2.0'
            fi
        fi

        command="${2}"
        shift 2
    # run 0.1.0 -- inko --help
    elif [[ "${2}" == '--' ]]
    then
        version="${1}"
        command="${3}"
        shift 3
    else
        error 'Invalid syntax, use "run [VERSION] -- [COMMAND]" or "run -- [COMMAND]" instead'
    fi

    if [[ "${version}" == '' ]]
    then
        error 'You must specify a version to use'
    fi

    if [[ "${version}" == 'latest' ]]
    then
        version="$(latest_installed_version)"
    fi

    verify_version_format "${version}"

    if [[ "${command}" == '' ]]
    then
        error 'You must specify a command to execute'
    fi

    local version_dir="${installs_dir}/${version}"
    local inkoc_path="${version_dir}/bin"

    if ! version_installed "${version}"
    then
        error "Version ${version} is not installed"
    fi

    # Our bin/ path must come first, otherwise we'll end up in an infinite loop
    # if the wrappers are also in PATH.
    PATH="${inkoc_path}:$PATH" exec "${command}" "$@"
}

# Downloads a package, printing the HTTP status.
function download_package() {
    local pkg="${1}"
    local file="${2}"
    local url="${mirror}/${pkg}"

    if [[ -f "${file}" ]]
    then
        echo '200'
    else
        local http_status
        http_status="$(curl --silent --write-out '%{http_code}' \
            --output "${file}" "${url}")"

        if [[ "${http_status}" != "200" ]]
        then
            # If the status wasn't HTTP 200 we most likely ended up downloading
            # something bogus (e.g. an error page).
            rm -f "${file}"
        fi

        echo "${http_status}"
    fi
}

# Unpacks a tar archive into a directory.
function extract_archive() {
    local file="${1}"
    local directory="${2}"

    info "Extracting ${file}"

    mkdir -p "${directory}"

    local output
    output="$(tar -C "${directory}" -xf "${file}" 2>&1)"

    # shellcheck disable=SC2181
    if [[ "$?" != '0' ]]
    then
        error "Failed to extract the archive:\\n${output}"
    fi
}

# Installs a package from source.
function install_source() {
    local version="${1}"
    local pkg="${2}"
    local pkg_file="${downloads_dir}/${pkg}"
    local staging_dir="${downloads_dir}/${version}"
    local install_dir="${installs_dir}/${version}"

    info "Downloading ${pkg}"

    local http_code
    http_code="$(download_package "${pkg}" "${pkg_file}")"

    if [[ "${http_code}" == "200" ]]
    then
        extract_archive "${pkg_file}" "${staging_dir}"

        info 'Compiling'

        local output
        output="$(cd "${staging_dir}" \
            && run_make build PREFIX="${install_dir}" \
            && run_make install PREFIX="${install_dir}" 2>&1)"

        # shellcheck disable=SC2181
        if [[ "$?" != "0" ]]
        then
            error "Installation failed:\\n${output}"
        fi

        info 'Cleaning up'

        rm -rf "${staging_dir}"

        info "Finished installing version ${version}"
    else
        error "Failed to download ${pkg}, HTTP response status was ${http_code}"
    fi
}

# Runs the program.
function run() {
    check_requirements
    create_directories

    if [[ $# -eq 0 ]]
    then
        usage
        exit
    fi

    while [[ $# -gt 0 ]]
    do
        case $1 in
            -h|--help)
                usage
                exit
                ;;
            -v|--version)
                version
                exit
                ;;
            install)
                local version="${2}"
                shift 2
                run_install "${version}"
                exit
                ;;
            remove)
                local version="${2}"
                shift 2
                run_remove "${version}"
                exit
                ;;
            list)
                shift
                run_list
                exit
                ;;
            known)
                shift
                run_known
                exit
                ;;
            default)
                local version="${2}"
                shift 2
                run_default "${version}"
                exit
                ;;
            run)
                local version="${2}"
                shift 2
                run_run "${version}" "$@"
                exit
                ;;
            clean)
                shift
                run_clean
                exit
                ;;
            *)
                error "Unexpected argument: ${1}"
                ;;
        esac
    done
}

run "$@"

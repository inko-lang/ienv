# Inko Version Manager

**NOTE:** this project has been replaced by
[ivm](https://gitlab.com/inko-lang/ivm), Inko's new cross-platform version
manager.

ienv lets you easily install and switch between multiple versions of Inko during
development. ienv can install Inko from source, or use a precompiled package
from <https://releases.inko-lang.org/>.

ienv is _not_ meant for production servers. Instead, use your operating system's
package manager, or build from source.

## Requirements

* Bash 4.0 or newer
* Curl
* Grep
* (GNU) coreutils
* (GNU) findutils
* autoconf
* automake
* clang
* libtool
* Make 4.0 or newer
* Rust 1.28 or newer
* Ruby 2.3 or newer (for the compiler)

## Installation

Clone the repository:

    git clone https://gitlab.com/inko-lang/ienv.git ~/.local/share/ienv

Now you need to add the `ienv` executable to your PATH for your shell of choice.
For Bash you would add the following to `~/.bashrc`:

    export PATH="${PATH}:$HOME/.local/share/ienv/bin"

For Fish, add the following to `~/.config/fish/config.fish`:

    set -x PATH $PATH $HOME/.local/share/ienv/bin

## Usage

Installing a version:

    ienv install 0.8.0

Setting the global default version to use:

    ienv default 0.8.0

Removing a version:

    ienv remove 0.8.0

Listing all installed versions:

    ienv list

Listing all available installations (this will require network access):

    ienv known

Running an Inko program using a specific version:

    ienv run 0.8.0 -- inko --version

You can also leave out the version, but only if a default version is specified
_or_ there is a directory specific override:

    ienv run -- inko --version

## Directory overrides

You can specify a directory specific version by creating a file called
`inko-version` in the target directory, and adding the version to it. For
example:

    echo '0.8.0' > inko-version

Currently ienv will _only_ look in the _current_ working directory for an
override, parent directories are not examined.

## Configuration

ienv can be configured using various environment variables.

### IENV_MIRROR

The base URL of the mirror to use for downloading packages. Defaults to
<https://releases.inko-lang.org/inko>.

## License

All source code in this repository is licensed under the Mozilla Public License
version 2.0, unless stated otherwise. A copy of this license can be found in the
file "LICENSE".
